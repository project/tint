<?php

namespace Drupal\tint\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Provides a 'TintEmbedBlock' block derivative.
 */
class TintEmbedBlockDerivative extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The tint settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs \Drupal\tint\Plugin\Derivative\TintEmbedBlockDerivative object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TranslationInterface $string_translation) {
    $this->config = $config_factory->get('tint.settings');
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $count = $this->config->get('tint_blocks');

    for ($delta = 1; $delta <= $count; $delta++) {
      $info = $this->t('TINT Embed HTML');
      $this->derivatives['tint_' . $delta] = $base_plugin_definition;
      $this->derivatives['tint_' . $delta]['admin_label'] = $this->t('@info', ['@info' => $info]);
    }
    return $this->derivatives;
  }

}
